<?php

//boolean example start here

$decision=true;
if ($decision){

    echo"The decision is True!<br>";
}

$decision=false;
if($decision){
    echo"The decision is false!";
}

//Boolean example end here

//Integer & Float example start here

$value1=100; //integer
$value2=56.35;//float
//Integer & Float example end here

//String example start here
$myString1='abcd1234#';
$myString2="Double Quoated Literal";

echo $myString1."<br>";
echo $myString2."<br>";

$heredocString=<<<BITM
heredoc line1 $value1 <br>
heredoc line2 $value1 <br>
heredoc line3 $value1 <br>
BITM;
echo $heredocString;

$nowdocString=<<<'BITM'
nowdoc line1 $value1 <br>
nowdoc line2 $value1 <br>
nowdoc line3 $value1 <br>
BITM;
echo $nowdocString;

//String example end here

//Array example start here
$indexedArray=array(1,2,3,4,5,6,7,55);
print_r($indexedArray);

$indexedArray=array("Toyota","Suzuki","BMW","Ford");
print_r($indexedArray);

$ageArray=array("Rahim"=>23,"Rafiq"=>40,"Salma"=>56,"Halim"=>32);
print_r($ageArray);
$ageArray['Rafiq']=45;
echo($ageArray['Rafiq']);
echo"<pre>";
print_r($ageArray);
echo"</pre>";
//Array example end here
?>