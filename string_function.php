<?php
$str="I am a student of 'BITM";
echo addslashes($str);
echo"<br>";
// Explode function
$arr=explode(" ",$str);
echo"<pre>";
print_r($arr);
echo"</pre>";

//Implode function
echo"<br>";
$str2=implode('","',$arr);
$str2='"'.$str2.'"';
echo $str2;

echo"<br>";
$str3="<br> is html tag";
echo "HTMLentities: ".htmlentities($str3);
echo"<br>";
//Remove characters from both side of a string

$newstring="Hello World!!";
echo $newstring."<br>";
echo "Trim: ".trim($newstring,"Hed");
echo"<br>";
//Remove characters from left side of a string

$newstring="Hello World!!";
echo $newstring."<br>";
echo "lTrim: ".ltrim($newstring,"Hello");
echo"<br>";

//Remove characters from right side of a string

$newstring="Hello World";
echo $newstring."<br>";
echo "rTrim: ".rtrim($newstring,"World");
echo"<br>";

echo nl2br("One line.\n Another Line");
echo"<br>";
echo"<br>";
$newstring1="Hello World";
echo str_pad($newstring1,20,"*");//Increase $newstring1 to 20 character length
echo"<br>";
echo str_repeat("Hi! ",10);

//String Replace
echo"<br>";
$myStr=" BASIS Institute of Technology and Management";
echo str_replace("o","x",$myStr);

echo"<br>";
$myArr=str_split($myStr,5);
print_r($myArr);

//Compare of Substring
echo"<br>";
$mainStr="Hello world! How is life!";
$str="Hello world! How is";
echo substr_compare($mainStr,$str,13);

//Substring count
echo"<br>";
$str2="l";
echo substr_count($mainStr,$str2);

echo"<br>";
$str2="hello world";
echo ucwords($str2);

?>